import IMask from 'imask'
import './css/index.css'

const ccCompanyLogo = document.querySelector('.cc-logo > span + span img')
const ccBgColor01 = document.querySelector('.cc-bg svg > g g:nth-child(1) path')
const ccBgColor02 = document.querySelector('.cc-bg svg > g g:nth-child(2) path')

function setCardType(type) {
  const colors = {
    default: ['black','gray'],
    mastercard: ['#C69347','#DF6F29'],
    visa: ['#2D57F2','#436D99'],
  }

  ccCompanyLogo.setAttribute('src', `cc-${type}.svg`)
  ccBgColor01.setAttribute('fill', colors[type][0])
  ccBgColor02.setAttribute('fill', colors[type][1])
}

const securityPatterns = {
  card: {
    mask: [
      {
        mask: '0000 0000 0000 0000',
        regex: /(^5[1-5]\d{0,2}|^22[2-9]\d|^2[3-7]\d{0,2})\d{0,12}/,
        cardType: 'mastercard',
      },
      {
        mask: '0000 0000 0000 0000',
        regex: /^4\d{0,15}/,
        cardType: 'visa',
      },
      {
        mask: '0000 0000 0000 0000',
        cardType: 'default',
      },
    ],
    dispatch: function (appended, dynamicMasked) {
      const number = (dynamicMasked.value + appended).replace(/\D/g, '')
      const validMask = dynamicMasked.compiledMasks.find(function (item) {
        return number.match(item.regex)
      })

      return validMask
    }
  },
  code: {
    mask: '0000'
  },
  date: {
    mask: 'MM{/}YY',
    blocks: {
      YY: {
        mask: IMask.MaskedRange,
        from: String(new Date().getFullYear()).slice(2),
        to: String(new Date().getFullYear() + 10).slice(2),
      },
      MM: {
        mask: IMask.MaskedRange,
        from: 1,
        to: 12,
      }
    }
  },
}

const cardNumber = document.querySelector('#card-number')
const expirationDate = document.querySelector('#expiration-date')
const securityCode = document.querySelector('#security-code')

const cardNumberMasked = IMask(cardNumber, securityPatterns.card)
const expirationDateMasked = IMask(expirationDate, securityPatterns.date)
const securityCodeMasked = IMask(securityCode, securityPatterns.code)

const addButton = document.querySelector('#add-card')
addButton.addEventListener('click', () => alert('cartão adicionado!'))
document.querySelector('form').addEventListener('submit', (event) => event.preventDefault())

const cardHolder = document.querySelector('#card-holder')
cardHolder.addEventListener('input', () => {
  const ccHolder = document.querySelector('.cc-holder .value')
  ccHolder.innerText = cardHolder.value.length ? cardHolder.value : 'Fulano da Silva'
})

cardNumberMasked.on('accept', () => {
  const cardFlag = cardNumberMasked.masked.currentMask.cardType
  setCardType(cardFlag)
  updateCardNumber(cardNumberMasked.value)
})
expirationDateMasked.on('accept', () => updateExpirationDate(expirationDateMasked.value))
securityCodeMasked.on('accept', () => updateSecurityCode(securityCodeMasked.value))

function updateCardNumber(number) {
  const ccNumber = document.querySelector('.cc-number')
  ccNumber.innerText = number.length ? number : '1234 5678 9012 3456'
}
function updateExpirationDate(date) {
  const ccExpiration = document.querySelector('.cc-expiration .value')
  ccExpiration.innerText = date.length ? date : '02/32'
}
function updateSecurityCode(code) {
  const ccSecurity = document.querySelector('.cc-security .value')
  ccSecurity.innerText = code.length ? code : '0123'
}
